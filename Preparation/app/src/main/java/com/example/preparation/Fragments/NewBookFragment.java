package com.example.preparation.Fragments;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.preparation.Database.AuthorRepository;
import com.example.preparation.Database.BookRepository;
import com.example.preparation.R;
import com.example.preparation.Utils.DatabaseConstants;

import java.util.ArrayList;

public class NewBookFragment extends Fragment {
    EditText name;
    EditText numberOfPages;
    EditText type;
    EditText genre;
    Spinner authorSpinner;
    int authorID;
    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    public NewBookFragment() {
        // Required empty public constructor
    }
    public static NewBookFragment newInstance() {
        return new NewBookFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookRepository = new BookRepository(getContext());
        authorRepository = new AuthorRepository(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.new_book, container, false);

    }

    @SuppressLint("Range")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        name = view.findViewById(R.id.new_name);
        numberOfPages = view.findViewById(R.id.new_numberOfPages);
        Log.i("ASD", String.valueOf(numberOfPages));
        type = view.findViewById(R.id.new_type);
        genre = view.findViewById(R.id.new_genre);
        authorSpinner = view.findViewById(R.id.new_author);
        String[] projection = { DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAMESURNAME,
                DatabaseConstants.COLUMN_BIRTHYEAR, DatabaseConstants.COLUMN_PLACEOFBIRTH};
        Cursor authors = authorRepository.getData(projection);
        ArrayList<String> names = new ArrayList<>();
        while (authors.moveToNext()){
            names.add(authors.getString(authors.getColumnIndex("nameSurname")));
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, names);
        // Specify the layout to use when the list of choices appears
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        authorSpinner.setAdapter(arrayAdapter);
        authorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.v("prepforma", String.valueOf(position));
                authors.moveToPosition(position);
                authorID = authors.getInt(authors.getColumnIndex("_id"));
                Log.v("prepforma", position + " " + authorID);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        Button confirm = view.findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewBook();
            }
        });

    }


    private void addNewBook() {

        bookRepository.insertData(name.getText().toString(), Integer.parseInt(numberOfPages.getText().toString()),
                type.getText().toString(), genre.getText().toString(), authorID);
//        ProductRepository productRepository = new ProductRepository(getContext());
//        productRepository.open();
//        productRepository.insertData(title, description, "image");
//        productRepository.close();

        requireActivity().getSupportFragmentManager().popBackStack();
    }
}
