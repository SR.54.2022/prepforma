package com.example.preparation.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import static com.example.preparation.Utils.DatabaseConstants.*;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "prepforma.db";
    //i pocetnu verziju baze. Obicno krece od 1
    private static final int DATABASE_VERSION = 1;
    private static SQLiteHelper sqLiteHelper;

    private static final String DB_CREATE_BOOKS = "create table "
            + TABLE_BOOKS + "("
            + COLUMN_ID  + " integer primary key autoincrement , "
            + COLUMN_NAME + " text, "
            + COLUMN_NUMBEROFPAGES + " integer, "
            + COLUMN_TYPE + " text, "
            + COLUMN_GENRE + " text, "
            + COLUMN_AUTHOR + " integer references AUTHORS(_id)"
            + ")";

    private static final String DB_CREATE_AUTHORS = "create table "
            + TABLE_AUTHORS + "("
            + COLUMN_ID  + " integer primary key autoincrement , "
            + COLUMN_NAMESURNAME + " text, "
            + COLUMN_BIRTHYEAR + " integer, "
            + COLUMN_PLACEOFBIRTH + " text"
            + ")";

    private static final String DB_CREATE_REVIEWS = "create table "
            + TABLE_REVIEWS + "("
            + COLUMN_ID  + " integer primary key autoincrement , "
            + COLUMN_NAMESURNAME + " text, "
            + COLUMN_COMMENT + " text, "
            + COLUMN_BOOK + " integer references BOOKS(_id)"
            + ")";

    /*
    * private static final String DB_CREATE_USERS = "create table "
            + TABLE_USERS + "("
            + COLUMN_ID  + " integer primary key autoincrement , "
            + COLUMN_USERNAME + " text, "
            + COLUMN_PASSWORD + " text, "
            + ")";
    * */

    //Potrebno je dodati konstruktor zbog pravilne inicijalizacije
    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public static SQLiteHelper getInstance(Context context) {

        if (sqLiteHelper == null) {
            synchronized (SQLiteHelper.class){ //thread safe singleton
                if (sqLiteHelper == null)
                    sqLiteHelper = new SQLiteHelper(context);
            }
//            Log.i("REZ_DB", "ON CREATE SQLITE HELPER");

        }

        return sqLiteHelper;
    }
    //Prilikom kreiranja baze potrebno je da pozovemo odgovarajuce metode biblioteke
    //prilikom kreiranja moramo pozvati db.execSQL za svaku tabelu koju imamo
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("REZ_DB", "ON CREATE SQLITE HELPER");
        db.execSQL(DB_CREATE_BOOKS);
        Log.i("REZ_DB", "ON CREATE SQLITE HELPER");

        db.execSQL(DB_CREATE_AUTHORS);
        Log.i("REZ_DB", "ON CREATE SQLITE HELPER");

        db.execSQL(DB_CREATE_REVIEWS);

        // db.execSQL(DB_CREATE_USERS);
    }

    //kada zelimo da izmenimo tabele, moramo pozvati drop table za sve tabele koje imamo
    //  moramo voditi računa o podacima, pa ćemo onda raditi ovde migracije po potrebi
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("REZ_DB", "ON UPGRADE SQLITE HELPER");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTHORS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REVIEWS);
        // db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(db);
    }
}
