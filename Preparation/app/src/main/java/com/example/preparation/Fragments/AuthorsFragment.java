package com.example.preparation.Fragments;

import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.preparation.Adapters.AuthorListAdapter;
import com.example.preparation.Database.AuthorRepository;
import com.example.preparation.Model.Author;
import com.example.preparation.R;
import com.example.preparation.Utils.DatabaseConstants;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AuthorsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AuthorsFragment extends ListFragment {

    private AuthorRepository authorRepository;
    private AuthorListAdapter authorListAdapter;

    public AuthorsFragment() {
        // Required empty public constructor
    }

    public static AuthorsFragment newInstance() {
        AuthorsFragment fragment = new AuthorsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authorRepository = new AuthorRepository(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_authors, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadAuthors();
    }

    private void loadAuthors() {
        String[] projection = { DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAMESURNAME,
                DatabaseConstants.COLUMN_BIRTHYEAR, DatabaseConstants.COLUMN_PLACEOFBIRTH};
        Cursor cursor = authorRepository.getData(projection);

        ArrayList<Author> authors = new ArrayList<>();

        if (cursor != null) {
            while (cursor.moveToNext()) {
                Author row = new Author();
                row.setID(Integer.parseInt(cursor.getString(0)));
                row.setNameSurname(cursor.getString(1));
                row.setBirthYear(cursor.getInt(2));
                row.setPlaceOfBirth(cursor.getString(3));
                authors.add(row);
            }
            authorListAdapter = new AuthorListAdapter(requireActivity(), authors);
            setListAdapter(authorListAdapter);
        }
        authorRepository.DBClose();
    }
}