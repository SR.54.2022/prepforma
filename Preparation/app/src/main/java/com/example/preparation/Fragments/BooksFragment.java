package com.example.preparation.Fragments;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.preparation.Adapters.BookListAdapter;
import com.example.preparation.Database.AuthorRepository;
import com.example.preparation.Database.BookRepository;
import com.example.preparation.Model.Author;
import com.example.preparation.Model.Book;
import com.example.preparation.R;
import com.example.preparation.Utils.DatabaseConstants;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BooksFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BooksFragment extends ListFragment {

    private BookRepository bookRepository;
    private BookListAdapter bookListAdapter;

    public BooksFragment() {
        // Required empty public constructor
    }
    public static BooksFragment newInstance() {
        return new BooksFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookRepository = new BookRepository(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_books, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadBooks();

        Button add = view.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransition.to(NewBookFragment.newInstance(), requireActivity(), true, R.id.fragmentContainer);
            }
        });
    }

    @SuppressLint("Range")
    private void loadBooks() {
        String[] projection = { DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAME, DatabaseConstants.COLUMN_NUMBEROFPAGES,
                DatabaseConstants.COLUMN_TYPE, DatabaseConstants.COLUMN_GENRE, DatabaseConstants.COLUMN_AUTHOR};
        Cursor cursor = bookRepository.getData(projection, null, null, null);

        ArrayList<Book> books = new ArrayList<>();

        if (cursor != null) {
            while (cursor.moveToNext()) {
                Book row = new Book();
                row.setID(Integer.parseInt(cursor.getString(0)));
                row.setName(cursor.getString(1));
                row.setNumberOfPages(cursor.getInt(2));
                row.setTypeOfBook(cursor.getString(3));
                row.setGenre(cursor.getString(4));

                AuthorRepository authorRepository = new AuthorRepository(getContext());
                Cursor cursor_a = authorRepository.getEntity(cursor.getInt(5));
                cursor_a.moveToNext();
                Author author = new Author(cursor_a.getInt(0),
                        cursor_a.getString(1), cursor_a.getInt(2),
                        cursor_a.getString(3));
                row.setAuthor(author);

                books.add(row);
            }
            bookListAdapter = new BookListAdapter(getActivity(), books);
            setListAdapter(bookListAdapter);
        }
        bookRepository.DBCLose();
    }
}