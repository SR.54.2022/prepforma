package com.example.preparation.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.example.preparation.Database.BookRepository;
import com.example.preparation.Model.Book;
import com.example.preparation.R;
import com.example.preparation.Utils.DatabaseConstants;
import java.util.ArrayList;

public class BookListAdapter extends ArrayAdapter<Book> {
    private ArrayList<Book> listOfBooks;
    private BookRepository bookRepository;

    public BookListAdapter(Context context, ArrayList<Book> books){
        super(context, R.layout.book_card, books);
        listOfBooks = books;
    }
    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return listOfBooks.size();
    }

    /*
     * Ova metoda vraca pojedinacan element na osnovu pozicije
     * */
    @Nullable
    @Override
    public Book getItem(int position) {
        return listOfBooks.get(position);
    }

    /*
     * Ova metoda vraca jedinstveni identifikator, za adaptere koji prikazuju
     * listu ili niz, pozicija je dovoljno dobra. Naravno mozemo iskoristiti i
     * jedinstveni identifikator objekta, ako on postoji.
     * */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        bookRepository = new BookRepository(getContext());

        Book book = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.book_card,
                    parent, false);
        }
        TextView name = convertView.findViewById(R.id.name);
        TextView numberOfPages = convertView.findViewById(R.id.numberOfPages);
        TextView type = convertView.findViewById(R.id.type);
        TextView genre = convertView.findViewById(R.id.genre);
        TextView author = convertView.findViewById(R.id.author);

        Button delete = convertView.findViewById(R.id.delButton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("DELETE", Long.toString(getItem(position).getID()));
                bookRepository.deleteData(getItem(position).getID());
                listOfBooks.remove(position);
                notifyDataSetChanged();
            }
        });

        if(book != null){
            name.setText("Name: " + book.getName());
            numberOfPages.setText("Number of pages: " + Integer.toString(book.getNumberOfPages()));
            type.setText("Type: " + book.getTypeOfBook());
            genre.setText("Genre: " + book.getGenre());
            author.setText("Author: " + book.getAuthor().getNameSurname());

            author.setOnClickListener(v -> {
                // Handle click on the item at 'position'
                String whereClause = DatabaseConstants.COLUMN_AUTHOR + " = ?";
                String[] whereArgs = {String.valueOf(book.getAuthor().getID())};
                Log.i("BOOKS: ", Integer.toString(book.getAuthor().getID()));

                Cursor cursor = bookRepository.getData(new String[]{DatabaseConstants.COLUMN_NAME}, whereClause, whereArgs, null);
                String authorNames = "";
                while (cursor.moveToNext()){
                    authorNames += cursor.getString(0) + "\r\n";
                }
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setMessage(authorNames)
                        .setCancelable(false)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                AlertDialog alert = dialog.create();
                alert.show();
            });
        }

        return convertView;
    }
}
