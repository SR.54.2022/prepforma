package com.example.preparation.Model;

public class Author {
    private int ID;
    private String nameSurname;
    private int birthYear;
    private String PlaceOfBirth;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getPlaceOfBirth() {
        return PlaceOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        PlaceOfBirth = placeOfBirth;
    }

    public Author() {
    }

    public Author(int ID, String nameSurname, int birthYear, String placeOfBirth) {
        this.ID = ID;
        this.nameSurname = nameSurname;
        this.birthYear = birthYear;
        PlaceOfBirth = placeOfBirth;
    }
}
