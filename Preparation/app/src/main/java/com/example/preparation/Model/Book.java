package com.example.preparation.Model;

public class Book {
    private int ID;

    private String name;
    private int numberOfPages;
    private String typeOfBook;
    private String genre;
    private Author author;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getTypeOfBook() {
        return typeOfBook;
    }

    public void setTypeOfBook(String typeOfBook) {
        this.typeOfBook = typeOfBook;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Book() {
    }

    public Book(int ID, String name, int numberOfPages, String typeOfBook, String genre, Author author) {
        this.ID = ID;
        this.name = name;
        this.numberOfPages = numberOfPages;
        this.typeOfBook = typeOfBook;
        this.genre = genre;
        this.author = author;
    }
}
