package com.example.preparation.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.preparation.Fragments.AllUsers;
import com.example.preparation.Fragments.FragmentTransition;
import com.example.preparation.R;

public class MainActivity extends AppCompatActivity {

    private String permission = Manifest.permission.CAMERA;

    private ActivityResultLauncher<String> mPermissionResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });


        mPermissionResult = registerForActivityResult(
                new ActivityResultContracts.RequestPermission(),
                result -> {
                    if (result) {
                        FragmentTransition.to(AllUsers.newInstance("S", "s"), this, false, R.id.ShowAllUsersContainer);
                    } else {
                        Toast.makeText(this, "Camera is not allowed!", Toast.LENGTH_SHORT).show();
                    }
                });

        Button askPermission = findViewById(R.id.askForPermission);
        askPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int getPermission = ContextCompat.checkSelfPermission(getApplicationContext(), permission);
                if (getPermission != PackageManager.PERMISSION_GRANTED) {
                    mPermissionResult.launch(permission);
                } else {
                    FragmentTransition.to(AllUsers.newInstance("S", "s"), MainActivity.this, false, R.id.ShowAllUsersContainer);
                }
            }
        });

        Button username = findViewById(R.id.showUsername);
        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ConstraintLayout) findViewById(R.id.main)).setBackgroundColor(Color.GRAY);
                TextView textView = findViewById(R.id.view);
                String loggedIn = getSharedPreferences("pref_file", MODE_PRIVATE).getString("pref_username", "default");
                String[] niz = loggedIn.split("\r\n");
                textView.setText(niz[niz.length - 1]);
            }
        });

        Button nextPage = findViewById(R.id.next);
        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Random.class);
                startActivity(intent);
            }
        });

        // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));


    }

    /*
     * onStart se poziva kada se aktivnost prvi put startuje, posle onCreate metode ili
     * kada se vratimo klikom na back dugme ponovo na aktivnost
     * */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Prep", "MainActivity onStart()");
    }

    /*
     * onResume se poziva kada je aktivnost u fokusu i korisnik
     * je u interakciji sa aktivnosti.
     * */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Prep", "MainActivity onResume()");
    }

    /*
     * onPause se poziva kada je aktivnost delimicno prekrivena.
     * */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Prep", "MainActivity onPause()");
    }

    /*
     * onStop se poziva kada je aktivnost u potpunosti prekrivena nekom drugom aktivnošću
     * */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Prep", "MainActivity onStop()");
    }

    /*
     * onDestory se poziva kada je aktivnost u potpunosti unistena,
     * ondosno kada je aplikacija zatvorena
     * Izbrisana je iz background-a.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Prep", "MainActivity onDestroy()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Prep", "MainActivity onRestart()");
    }

  /*  // Requesting permission to INTERNET and RECORD AUDIO
    private boolean isPermissions = true;
    private String [] permissions = {
            Manifest.permission.INTERNET,
            Manifest.permission.RECORD_AUDIO
    };
    private static final int REQUEST_PERMISSIONS = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        *//*
     * Fragmenti ne mogu da postoje samostalno, njih lepimo na aktivnosti.
     * Pogledati detalje ovih metoda.
     * *//*
        Button btnFragment1 = findViewById(R.id.btnFragment1);
        btnFragment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(FirstFragment.newInstance("Fragment 1", "Ovo je fragment 1"), CartActivity.this, false, R.id.downView);
            }
        });
        Button btnFragment2 = findViewById(R.id.btnFragment2);
        btnFragment2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransition.to(SecondFragment.newInstance("Fragment 2", "Ovo je fragment 2"), CartActivity.this, false, R.id.downView);
            }
        });
        *//*
     * Proveravanje prava pristupa
     * *//*
        onRequestPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case REQUEST_PERMISSIONS:
                for(int i = 0; i < permissions.length; i++) {
                    Log.i("ShopApp", "permission " + permissions[i] + " " + grantResults[i]);
                    if(grantResults[i] == PackageManager.PERMISSION_DENIED){
                        isPermissions = false;
                    }
                }
                break;
        }

        if (!isPermissions) {
            Log.e("ShopApp", "Error: no permission");
            finishAndRemoveTask();
        }

    }

    private void onRequestPermission(){
        Log.i("ShopApp", "onRequestPermission");
        ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSIONS);
    }
*/

}