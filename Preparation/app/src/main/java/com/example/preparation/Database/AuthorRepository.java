package com.example.preparation.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.preparation.Utils.DatabaseConstants;

public class AuthorRepository {
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    public AuthorRepository(Context context) {
        dbHelper = SQLiteHelper.getInstance(context);
    }

    // Insert metoda
    public long insertData(String nameSurname, int birthYear, String placeOfBirth) {
        database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COLUMN_NAMESURNAME, nameSurname);
        values.put(DatabaseConstants.COLUMN_BIRTHYEAR, birthYear);
        values.put(DatabaseConstants.COLUMN_PLACEOFBIRTH, placeOfBirth);

        return database.insert(DatabaseConstants.TABLE_AUTHORS, null, values);

    }
    // Get metoda
    public Cursor getData(String[] projection) {
        database = dbHelper.getWritableDatabase();
        return database.query(DatabaseConstants.TABLE_AUTHORS, projection, null, null, null, null, null);
    }

    // Update metoda
    public int updateData(int id, String nameSurname, int birthYear, String placeOfBirth) {
        database = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COLUMN_NAMESURNAME, nameSurname);
        values.put(DatabaseConstants.COLUMN_BIRTHYEAR, birthYear);
        values.put(DatabaseConstants.COLUMN_PLACEOFBIRTH, placeOfBirth);

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.update(DatabaseConstants.TABLE_AUTHORS, values, whereClause, whereArgs);

    }

    // Delete metoda
    public int deleteData(int id) {
        database = dbHelper.getWritableDatabase();

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.delete(DatabaseConstants.TABLE_AUTHORS, whereClause, whereArgs);

    }
    public Cursor getEntity(int id) {
        database = dbHelper.getWritableDatabase();
        String[] projection = new String[]{DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAMESURNAME,
                DatabaseConstants.COLUMN_BIRTHYEAR,DatabaseConstants.COLUMN_PLACEOFBIRTH};

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.query(DatabaseConstants.TABLE_AUTHORS, projection, whereClause, whereArgs, null, null, null, null);

    }
    public void DBClose(){
        dbHelper.close();
    }
}
