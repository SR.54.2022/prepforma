package com.example.preparation.Utils;

public class DatabaseConstants {
    public static final String TABLE_BOOKS = "BOOKS";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_NUMBEROFPAGES = "numberOfPages";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_GENRE = "genre";
    public static final String COLUMN_AUTHOR = "author";

    public static final String TABLE_AUTHORS = "AUTHORS";
    public static final String COLUMN_NAMESURNAME = "nameSurname";
    public static final String COLUMN_BIRTHYEAR = "birthYear";
    public static final String COLUMN_PLACEOFBIRTH = "placeOfBirth";

    public static final String TABLE_REVIEWS = "REVIEWS";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_BOOK = "book";

    /*
    * public static final String TABLE_USER = "USERS";
    * public static final String COLUMN_USERNAME = "username";
    * public static final String COLUMN_PASSWORD = "password";
    * */
}
