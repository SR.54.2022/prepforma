package com.example.preparation.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.preparation.Database.ReviewRepository;
import com.example.preparation.Model.Review;
import com.example.preparation.R;

import java.util.ArrayList;

public class ReviewListAdapter extends ArrayAdapter<Review> {
    private ArrayList<Review> listOfReviews;
    private ReviewRepository reviewRepository;

    public ReviewListAdapter(Context context, ArrayList<Review> reviews){
        super(context, R.layout.review_card, reviews);
        listOfReviews = reviews;
    }
    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return listOfReviews.size();
    }

    /*
     * Ova metoda vraca pojedinacan element na osnovu pozicije
     * */
    @Nullable
    @Override
    public Review getItem(int position) {
        return listOfReviews.get(position);
    }

    /*
     * Ova metoda vraca jedinstveni identifikator, za adaptere koji prikazuju
     * listu ili niz, pozicija je dovoljno dobra. Naravno mozemo iskoristiti i
     * jedinstveni identifikator objekta, ako on postoji.
     * */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        reviewRepository = new ReviewRepository(getContext());

        Review review = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.review_card,
                    parent, false);
        }
        TextView nameSurname = convertView.findViewById(R.id.nameSurname);
        TextView comment = convertView.findViewById(R.id.comment);
        TextView name = convertView.findViewById(R.id.name);

        if(review != null){
            nameSurname.setText("Name and surname: " + review.getNameSurname());
            comment.setText("Comment: " + review.getComment());
            name.setText("Book: " + review.getBook().getName());
        }
        return convertView;
    }
}
