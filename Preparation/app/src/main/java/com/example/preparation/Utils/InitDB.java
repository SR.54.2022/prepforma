package com.example.preparation.Utils;

import android.app.Activity;
import android.util.Log;

import com.example.preparation.Database.AuthorRepository;

public class InitDB {
    public static void initDB(Activity activity) {
        AuthorRepository authorRepository = new AuthorRepository(activity.getApplicationContext());
        Log.i("REZ_DB", "ENTRY INSERT TO DATABASE");
        {
            String[] projection = { DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAMESURNAME,
                    DatabaseConstants.COLUMN_BIRTHYEAR, DatabaseConstants.COLUMN_PLACEOFBIRTH};
            if (authorRepository.getData(projection).getCount() == 0) {
                authorRepository.insertData("Mihail Bulgakov", 1891, "Kijev");
                authorRepository.insertData("Fjodor Dostojevski", 1821, "Moskva");
            }
        }


        /*
        * UserRepository userRepository = new UserRepository(activity.getApplicationContext());
        Log.i("REZ_DB", "ENTRY INSERT TO DATABASE");
        {
            String[] projection = { DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_USERNAME,
                    DatabaseConstants.COLUMN_PASSWORD};
            if (userRepository.getData(projection).getCount() == 0) {
                userRepository.insertData("pera", pera123);
                userRepository.insertData("mika", mika321);
            }
        }
        *
        * */
    }
}
