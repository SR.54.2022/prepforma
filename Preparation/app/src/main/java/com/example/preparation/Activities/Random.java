package com.example.preparation.Activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.preparation.Fragments.AuthorsFragment;
import com.example.preparation.Fragments.BooksFragment;
import com.example.preparation.Fragments.FragmentTransition;
import com.example.preparation.Fragments.ReviewsFragment;
import com.example.preparation.R;
import com.example.preparation.Utils.InitDB;

import java.util.Objects;

public class Random extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_random);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        InitDB.initDB(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch ((Objects.requireNonNull(item.getTitle())).toString()) {
            case "Books":
                // User chooses the "Settings" item. Show the app settings UI.
                FragmentTransition.to(BooksFragment.newInstance(), Random.this, true, R.id.fragmentContainer);
                return true;

            case "Authors":
                FragmentTransition.to(AuthorsFragment.newInstance(), Random.this, true, R.id.fragmentContainer);

                return true;
            case "Reviews":
                FragmentTransition.to(ReviewsFragment.newInstance(), Random.this, true, R.id.fragmentContainer);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}