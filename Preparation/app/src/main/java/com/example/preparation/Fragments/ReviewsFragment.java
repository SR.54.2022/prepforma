package com.example.preparation.Fragments;

import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.preparation.Adapters.ReviewListAdapter;
import com.example.preparation.Database.BookRepository;
import com.example.preparation.Database.ReviewRepository;
import com.example.preparation.Model.Author;
import com.example.preparation.Model.Book;
import com.example.preparation.Model.Review;
import com.example.preparation.R;
import com.example.preparation.Utils.DatabaseConstants;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReviewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReviewsFragment extends ListFragment {

    private ReviewRepository reviewRepository;
    private ReviewListAdapter reviewListAdapter;
    private ArrayList<Review> reviews;

    public ReviewsFragment() {
        // Required empty public constructor
    }

    public static ReviewsFragment newInstance() {
        return new ReviewsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reviewRepository = new ReviewRepository(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reviews, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button add = view.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransition.to(NewReviewFragment.newInstance(), requireActivity(), true, R.id.fragmentContainer);
            }
        });

        loadReviews();

        EditText searchBar = view.findViewById(R.id.searchBar);
        Button search = view.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Review> res = (ArrayList<Review>) reviews.stream()
                        .filter(s -> s.getBook().getName().equals(search.getText().toString()))
                        .collect(Collectors.toList());
                if (search.getText().toString().equals("")){
                    reviewListAdapter = new ReviewListAdapter(getActivity(), reviews);
                } else {
                    reviewListAdapter = new ReviewListAdapter(getActivity(), res);
                }
                setListAdapter(reviewListAdapter);
            }
        });

    }

    private void loadReviews() {
        String[] projection = { DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAMESURNAME,
                DatabaseConstants.COLUMN_COMMENT, DatabaseConstants.COLUMN_BOOK};
        Cursor cursor = reviewRepository.getData(projection, null, null, null);

        reviews = new ArrayList<>();

        if (cursor != null) {
            while (cursor.moveToNext()) {
                Review row = new Review();
                row.setID(Integer.parseInt(cursor.getString(0)));
                row.setNameSurname(cursor.getString(1));
                row.setComment(cursor.getString(2));

                BookRepository bookRepository = new BookRepository(getContext());
                Cursor cursor_k = bookRepository.getEntity(cursor.getInt(3));
                if (cursor_k.getCount() != 0) {
                    cursor_k.moveToNext();
                    Book book = new Book(cursor_k.getInt(0),
                            cursor_k.getString(1), cursor_k.getInt(2),
                            cursor_k.getString(3), cursor_k.getString(4), new Author());
                    row.setBook(book);
                    reviews.add(row);
                }
            }
            reviewListAdapter = new ReviewListAdapter(getActivity(), reviews);
            setListAdapter(reviewListAdapter);
        }
        reviewRepository.DBClose();
    }
}