package com.example.preparation.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.example.preparation.Database.BookRepository;
import com.example.preparation.Model.Author;
import com.example.preparation.R;
import com.example.preparation.Utils.DatabaseConstants;

import java.util.ArrayList;

public class AuthorListAdapter extends ArrayAdapter<Author> {
    private ArrayList<Author> listOfAuthors;
    private BookRepository bookRepository;
    public AuthorListAdapter(@NonNull Context context, @NonNull ArrayList<Author> authors) {
        super(context, R.layout.author_card, authors);
        listOfAuthors = authors;
    }

    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return listOfAuthors.size();
    }

    /*
     * Ova metoda vraca pojedinacan element na osnovu pozicije
     * */
    @Nullable
    @Override
    public Author getItem(int position) {
        return listOfAuthors.get(position);
    }

    /*
     * Ova metoda vraca jedinstveni identifikator, za adaptere koji prikazuju
     * listu ili niz, pozicija je dovoljno dobra. Naravno mozemo iskoristiti i
     * jedinstveni identifikator objekta, ako on postoji.
     * */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        bookRepository = new BookRepository(getContext());
        Author author = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.author_card,
                    parent, false);
        }
        TextView nameSurname = convertView.findViewById(R.id.nameSurname);
        TextView birthYear = convertView.findViewById(R.id.birthYear);
        TextView placeOfBirth = convertView.findViewById(R.id.placeOfBirth);
        LinearLayout authorCard = convertView.findViewById(R.id.authorCardItem);

        authorCard.setOnClickListener(v -> {
            // Handle click on the item at 'position'
            String whereClause = DatabaseConstants.COLUMN_AUTHOR + " = ?";
            String[] whereArgs = {String.valueOf(author.getID())};

            Cursor cursor = bookRepository.getData(new String[]{DatabaseConstants.COLUMN_NAME}, whereClause, whereArgs, null);
            String authors = "";
            while (cursor.moveToNext()){
                authors += cursor.getString(0) + "\r\n";
            }
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
            dialog.setMessage(authors)
                    .setCancelable(false)
                    .setPositiveButton("ok", (dialogInterface, i) -> {

                    });
            AlertDialog alert = dialog.create();
            alert.show();
        });

        if(author != null){
            nameSurname.setText(author.getNameSurname());
            birthYear.setText("Birth Year: " + author.getBirthYear());
            placeOfBirth.setText("Place Of Birth: " + author.getPlaceOfBirth());
        }

        return convertView;
    }
}
