/*
package com.example.preparation.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.preparation.Database.ReviewRepository;
import com.example.preparation.Model.Review;
import com.example.preparation.R;

import java.util.ArrayList;

public class UserListAdapter extends ArrayAdapter<User> {
    private ArrayList<User> listOfUsers;
    private UserRepository userRepository;

    public UserListAdapter(Context context, ArrayList<Review> reviews){
        super(context, R.layout.review_card, reviews);
        listOfReviews = reviews;
    }

    @Override
    public int getCount() {
        return listOfUsers.size();
    }


    @Nullable
    @Override
    public User getItem(int position) {
        return listOfUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        userRepository = new UserRepository(getContext());

        User user = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.review_card,
                    parent, false);
        }
        TextView username = convertView.findViewById(R.id.nameSurname);
        TextView password = convertView.findViewById(R.id.comment);

        if(user != null){
            username.setText("Username: " + user.getUsername());
            password.setText("Passowrd: " + user.getPassword());
        }
        return convertView;
    }
}

*/
