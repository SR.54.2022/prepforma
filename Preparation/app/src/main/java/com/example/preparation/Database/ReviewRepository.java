package com.example.preparation.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.preparation.Utils.DatabaseConstants;

public class ReviewRepository {
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    public ReviewRepository(Context context) {
        dbHelper = SQLiteHelper.getInstance(context);
    }

    // Insert metoda
    public long insertData(String nameSurname, String comment, int bookID) {
        database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COLUMN_NAMESURNAME, nameSurname);
        values.put(DatabaseConstants.COLUMN_COMMENT, comment);
        values.put(DatabaseConstants.COLUMN_BOOK, bookID);

        return database.insert(DatabaseConstants.TABLE_REVIEWS, null, values);

    }
    // Get metoda
    public Cursor getData(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        database = dbHelper.getWritableDatabase();
        return database.query(DatabaseConstants.TABLE_REVIEWS, projection, selection, selectionArgs, null, null, null);

    }

    /*
    * public List<String> getAllReviewsWithUsername() {
        List<String> reviews = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DatabaseConstants.TABLE_REVIEWS, new String[]{"review_text"}, null, null, null, null, null);

        String username = getUsernameFromPreferences();  // Get the username from SharedPreferences

        if (cursor.moveToFirst()) {
            do {
                String review = cursor.getString(cursor.getColumnIndex("review_text"));
                reviews.add(username + ": " + review);  // Append username to each review
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return reviews;
    }

    private String getUsernameFromPreferences() {
        SharedPreferences prefs = context.getSharedPreferences("UserSession", Context.MODE_PRIVATE);
        return prefs.getString("Username", "Unknown");
    }
    *
    * SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("pref_file", Context.MODE_PRIVATE);
        sharedPreferences.getString("pref_username", "default");
    * */

    // Update metoda
    public int updateData(int id, String nameSurname, int comment, int bookID) {
        database = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COLUMN_NAMESURNAME, nameSurname);
        values.put(DatabaseConstants.COLUMN_COMMENT, comment);
        values.put(DatabaseConstants.COLUMN_BOOK, bookID);

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.update(DatabaseConstants.TABLE_REVIEWS, values, whereClause, whereArgs);

    }

    // Delete metoda
    public int deleteData(int id) {
        database = dbHelper.getWritableDatabase();

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.delete(DatabaseConstants.TABLE_REVIEWS, whereClause, whereArgs);

    }

    public Cursor getEntity(int id) {
        database = dbHelper.getWritableDatabase();
        String[] projection = new String[]{DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAMESURNAME,
                DatabaseConstants.COLUMN_COMMENT,DatabaseConstants.COLUMN_BOOK};

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.query(DatabaseConstants.TABLE_REVIEWS, projection, whereClause, whereArgs, null, null, null, null);

    }

    public void DBClose(){
        dbHelper.close();
    }
}
