package com.example.preparation.Model;

public class Review {
    private int ID;
    private String nameSurname;
    private String comment;
    private Book book;

    public Review(int id, String nameSurname, String comment, Book book) {
        this.ID = id;
        this.nameSurname = nameSurname;
        this.comment = comment;
        this.book = book;
    }

    public Review() {

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
