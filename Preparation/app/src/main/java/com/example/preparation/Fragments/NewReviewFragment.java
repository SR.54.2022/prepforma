package com.example.preparation.Fragments;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.preparation.Database.BookRepository;
import com.example.preparation.Database.ReviewRepository;
import com.example.preparation.R;
import com.example.preparation.Utils.DatabaseConstants;

import java.util.ArrayList;

public class NewReviewFragment extends Fragment {
    EditText nameSurname;
    EditText comment;
    int bookID;
    Spinner bookSpinner;
    private ReviewRepository reviewRepository;
    private BookRepository bookRepository;
    public NewReviewFragment() {
        // Required empty public constructor
    }

    public static NewReviewFragment newInstance() {
        return new NewReviewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookRepository = new BookRepository(getContext());
        reviewRepository = new ReviewRepository(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.new_review, container, false);
    }

    @SuppressLint("Range")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nameSurname = view.findViewById(R.id.new_nameSurname);
        comment = view.findViewById(R.id.new_comment);
        bookSpinner = view.findViewById(R.id.new_name);
        Cursor cursor = bookRepository.getData(new String[]{DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAME}, null, null, null);
        ArrayList<String> names = new ArrayList<>();
        while (cursor.moveToNext()){
            names.add(cursor.getString(cursor.getColumnIndex("name")));
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(requireActivity(),
                android.R.layout.simple_spinner_item, names);
        // Specify the layout to use when the list of choices appears
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        bookSpinner.setAdapter(arrayAdapter);
        bookSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToPosition(position);
                bookID = cursor.getInt(cursor.getColumnIndex("_id"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        Button confirm = view.findViewById(R.id.confirm1);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewReview();
            }
        });
    }

    private void addNewReview() {

        reviewRepository.insertData(nameSurname.getText().toString(),
                comment.getText().toString(), bookID);
        requireActivity().getSupportFragmentManager().popBackStack();
    }
}
