package com.example.preparation.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.preparation.Utils.DatabaseConstants;

public class BookRepository {
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    public BookRepository(Context context) {
        dbHelper = SQLiteHelper.getInstance(context);
    }

    // Insert metoda
    public long insertData(String name, int numberOfPages, String type, String genre, int authorID) {
        database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COLUMN_NAME, name);
        values.put(DatabaseConstants.COLUMN_NUMBEROFPAGES, numberOfPages);
        values.put(DatabaseConstants.COLUMN_TYPE, type);
        values.put(DatabaseConstants.COLUMN_GENRE, genre);
        values.put(DatabaseConstants.COLUMN_AUTHOR, authorID);

        return database.insert(DatabaseConstants.TABLE_BOOKS, null, values);
    }
    // Get metoda
    public Cursor getData(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        database = dbHelper.getWritableDatabase();

        return database.query(DatabaseConstants.TABLE_BOOKS, projection, selection, selectionArgs, null, null, null);

    }

    // Update metoda
    public int updateData(int id, String name, int numberOfPages, String type, String genre, int authorID) {
        database = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COLUMN_NAME, name);
        values.put(DatabaseConstants.COLUMN_NUMBEROFPAGES, numberOfPages);
        values.put(DatabaseConstants.COLUMN_TYPE, type);
        values.put(DatabaseConstants.COLUMN_GENRE, genre);
        values.put(DatabaseConstants.COLUMN_AUTHOR, authorID);

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.update(DatabaseConstants.TABLE_BOOKS, values, whereClause, whereArgs);

    }

    // Delete metoda
    public int deleteData(int id) {
        database = dbHelper.getWritableDatabase();

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.delete(DatabaseConstants.TABLE_BOOKS, whereClause, whereArgs);

    }
    public Cursor getEntity(int id) {
        database = dbHelper.getWritableDatabase();
        String[] projection = new String[]{DatabaseConstants.COLUMN_ID, DatabaseConstants.COLUMN_NAME, DatabaseConstants.COLUMN_NUMBEROFPAGES,
                DatabaseConstants.COLUMN_TYPE, DatabaseConstants.COLUMN_GENRE, DatabaseConstants.COLUMN_AUTHOR};

        String whereClause = DatabaseConstants.COLUMN_ID + " = ?";
        String[] whereArgs = {String.valueOf(id)};
        return database.query(DatabaseConstants.TABLE_BOOKS, projection, whereClause, whereArgs, null, null, null, null);

    }

    public void DBCLose(){
        dbHelper.close();
    }
}
